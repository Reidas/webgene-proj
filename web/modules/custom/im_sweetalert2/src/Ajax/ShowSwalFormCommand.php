<?php

namespace Drupal\im_sweetalert2\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Ajax\CommandWithAttachedAssetsTrait;

/**
 * AJAX command for invoking an arbitrary jQuery method.
 *
 * The 'invoke' command will instruct the client to invoke the given jQuery
 * method with the supplied arguments on the elements matched by the given
 * selector. Intended for simple jQuery commands, such as attr(), addClass(),
 * removeClass(), toggleClass(), etc.
 *
 * This command is implemented by Drupal.AjaxCommands.prototype.invoke()
 * defined in misc/ajax.js.
 *
 * @ingroup ajax
 */
class ShowSwalFormCommand implements CommandInterface {

  use CommandWithAttachedAssetsTrait;

  /**
   * The content for the dialog.
   *
   * Either a render array or an HTML string.
   *
   * @var string|array
   */
  protected $content;

  protected $dialogOptions = [];

  /**
   * A settings array to be passed to any attached JavaScript behavior.
   *
   * @var array
   */
  protected $settings;

  /**
   * Constructs an InvokeCommand object.
   *
   * @param string $selector
   *   A jQuery selector.
   * @param string $method
   *   The name of a jQuery method to invoke.
   * @param array $arguments
   *   An optional array of arguments to pass to the method.
   */
  public function __construct($conntent, $dialog_options, array $settings = NULL) {
    $this->content = $conntent;
    $this->dialogOptions = $dialog_options;
    $this->settings = $settings;
  }

  /**
   * Implements \Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render() {
    // For consistency ensure the modal option is set to TRUE or FALSE.
    $this->dialogOptions['modal'] = isset($this->dialogOptions['modal']) && $this->dialogOptions['modal'];
    return [
      'command' => 'showSwalForm',
      'data' => $this->getRenderedContent(),
      'dialogOptions' => $this->dialogOptions,
      'settings' => $this->settings,
    ];
  }

}
