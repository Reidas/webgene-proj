<?php
namespace Drupal\im_sweetalert2\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformSubmissionForm;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\helper_module\Ajax\ShowDialogCommand;
use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Form\FormState;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\helper_module\Ajax\ShowPopupFormCommand;
use Drupal\im_sweetalert2\Ajax\ShowSwalFormCommand;

class WebFormAjaxSubmitterController extends ControllerBase {

  public function redirectToWebformPage(WebformInterface $webform) {

    $webform_url = $webform->toUrl()->toString();

    return new RedirectResponse($webform_url);
  }

  public function getRederedForm(WebformInterface $webform) {

    $response = new AjaxResponse();

    $form = [
      '#type' => 'webform',
      '#webform' => $webform->id(),
      '#action' => '/ajax/im-swal/webforms/form-submit/' . $webform->id(),
    ];

    $rendered_form = \Drupal::service('renderer')->renderRoot($form);

    /**
     * @var FormBuilder $form_builder
     */
    $form_builder = \Drupal::formBuilder();

    $data['title'] = $webform->get('title');
    $data['customClass'] = "form webform webform-{$webform->id()}";

    if (!empty($form['#attached'])) {
      $response->setAttachments($form['#attached']);
    }

    $response->addCommand(new ShowSwalFormCommand($rendered_form, $data));

    return $response;
  }

  public function submitFormAjax(WebformInterface $webform) {
    $request = \Drupal::request();
    $response = new JsonResponse();

    $submission_form = $webform->getSubmissionForm();

    $result = [
      'status' => 'error',
    ];

    if (empty($submission_form['#children_errors'])) {
      $result['status'] = 'success';

      $result['message_title'] = $webform->getSetting('confirmation_title');
      $result['message'] = $webform->getSetting('confirmation_message');
    }

//     /**
//      * @var FormBuilder $form_builder
//      */
//     $form_builder = \Drupal::formBuilder();

//     $form_state = new FormState();
//     $form = $form_builder->getCache($request->get('form_build_id'), $form_state);

//     if ($webform_id = $request->get('webform_id')) {

//       $webform = Webform::load($webform_id);
//       $is_open = WebformSubmissionForm::isOpen($webform);

//       $values = [
//         'webform_id' => $webform_id,
//         'entity_type' => NULL,
//         'entity_id' => NULL,
//         'in_draft' => FALSE,
//         'uid' => '0',
// //         'langcode' => 'en',
// //         'token' => 'pgmJREX2l4geg2RGFp0p78Qdfm1ksLxe6IlZ-mN9GZI',
//         'uri' => '/webform/my_webform/api',
//         'remote_addr' => '',
//         'data' => [
//           'phone' => $request->get('phone'),
//           'name' => $request->get('name'),
//           'want_know_about_discounts' => $request->get('want_know_about_discounts'),
//         ],
//       ];

//       //foreach ($request->)

//       $webform_submission = WebformSubmission::create($values);
//       $webform_submission = WebformSubmissionForm::submitWebformSubmission($webform_submission);

//       if ($confirm_message = $webform->getSetting('confirmation_message')) {
//         $result['message'] = $confirm_message;
//       }

//       if ($confirm_message_title = $webform->getSetting('confirmation_title')) {
//         $result['message_title'] = $confirm_message_title;
//       }

//       $result['status'] = 'success';
//     }



    $response->setData($result);

    return $response;
  }

}

