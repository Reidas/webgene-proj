/**
 * 
 */
(function (domready, Drupal, drupalSettings) {
	
  DataSetOptionToSwalOption = function(name) {
    if (!name.match(/^swal/i)) return false;
    
    return name
    .replace(/^swal/g, '')
    .replace(/^(.)/, function($1) { return $1.toLowerCase(); });
  }
	
	domready(function () {
		
		document.querySelectorAll('.swal').forEach(function (element) {
			
		  var options = {
		      showConfirmButton: false,
          showCancelButton: false,
//          customClass: 'pre-loading-content',
          showCloseButton: true,
//          onBeforeOpen: () => {
//            Swal.showLoading();
//          },
          allowOutsideClick: () => !Swal.isLoading(),
          //showLoaderOnConfirm: true,
          title: false,
          html: false,
          'onClose': function (swalElement) {
            var event = new CustomEvent('swal:close', {
              detail: { args: swalElement }
            });
            window.dispatchEvent(event);
          },
		  };
      
      for (var dsOptionName in element.dataset) {
        var optionName = DataSetOptionToSwalOption(dsOptionName)
        if (optionName) {
          options[optionName] = element.dataset[dsOptionName];
        }
      }
      
      if (typeof(options['htmlContentSelector']) != undefined) {
        
        try {
          //options['html'] = document.querySelector(options['htmlContentSelector']).innerHTML;
        }
        finally {
          delete options['htmlContentSelector'];
        }
        
      }
		  
			element.addEventListener('click', function (event) {
				  event.preventDefault();
				  var html;
				  
				  if (typeof event.currentTarget.dataset['swalHtmlContentSelector'] != 'undefined') {
				    var htmlContentContainer = document.querySelector(event.currentTarget.dataset['swalHtmlContentSelector']);  
				  
				    if (htmlContentContainer) {
				      options['html'] = htmlContentContainer.innerHTML;
				    }
				    
				  }
				  
				  Swal.fire(options);
				
			});
		});
	});
	
})(domready, Drupal, window.drupalSettings);
