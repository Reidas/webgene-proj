/**
 * 
 */
(function (domready, Drupal, drupalSettings) {
	
	
	domready(function () {
		
		document.querySelectorAll('a[data-swal-webform]').forEach(function (link) {
			
			link.addEventListener('click', function (event) {

				if (typeof link.dataset.swalWebform != 'undefined') {
				  event.preventDefault();
				  
					Swal.fire({
	  				title: false,
	  			  html: false,
	  			  showConfirmButton: false,
	  			  showCancelButton: false,
	  			  customClass: 'pre-loading-content',
	  			  showCloseButton: true,
	  			  allowOutsideClick: () => !Swal.isLoading(),
	  			  showLoaderOnConfirm: true,
	  			  onBeforeOpen: () => {
	  			  	Swal.showLoading();
	  			  },
  			  });
				}
				
			});
		});
	});
	
	Drupal.behaviors.popupForm = {
    attach: function (context) {
      context.querySelectorAll('a[data-swal-webform]').forEach(function (linkElement) {
        
        if (typeof linkElement.dataset.swalWebform != 'undefined') {
          var elementSettings = {
            progress: { type: 'throbber' },
            base: linkElement.getAttribute('id'),
            element: linkElement,
            url: drupalSettings.path.baseUrl + drupalSettings.path.pathPrefix + 'ajax/im-swal/webform/' + linkElement.dataset.swalWebform + linkElement.search,
            event: 'click',
          };

          Drupal.ajax(elementSettings);
        }
        
      });
    }
	};
	
})(domready, Drupal, window.drupalSettings);
