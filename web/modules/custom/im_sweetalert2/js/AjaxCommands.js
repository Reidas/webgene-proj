/**
 * 
 */
/**
 * 
 */
(function (Drupal, drupalSettings) {
  
  var currentForm = null;
  
  async function submitWebForm(form, url) {
    return new Promise(function (resolve, reject) {
      // do the usual Http request
      let request = new XMLHttpRequest();
      request.open('POST', url);
     
      request.onload = function () {
          if (request.status == 200) {
            
            try {
              let result = JSON.parse(request.response);
              resolve(result);
            } catch (e) {
              Swal.showValidationMessage(
                  "Request failed"
              );
              resolve(false);
            }
            
          } else {
            Swal.showValidationMessage(
                "Request failed"
            )
            resolve(false);
          }
      };

      request.onerror = function () {
          reject(Error('Network Error'));
      };

      request.send(new FormData(form));
    });
  }
  
  Drupal.AjaxCommands.prototype.showSwalForm = function (ajax, response, status) {
    
    var options = Object.assign({
      html: response.data,
      showConfirmButton: false,
      showCancelButton: false,
      showCloseButton: true,
      allowOutsideClick: () => !Swal.isLoading(),
      showLoaderOnConfirm: true,
      onOpen: modal => {
        Drupal.attachBehaviors(modal, drupalSettings);
        
        modal.querySelector('form').addEventListener('submit', e => {
          e.preventDefault();
          currentForm = e.target;
          Swal.clickConfirm();
        });
      },
      preConfirm: () => {
        return submitWebForm(currentForm, currentForm.action);
      },
    }, response.dialogOptions);
    
    Swal.fire(options)
      .then((result) => {
        
        if (typeof result.dismiss != 'undefined') {
          return;
        }
        
        if (result.value.status == 'success') {
          Swal.fire({
            title: typeof result.value.message_title != 'undefined' ? 
                    result.value.message_title :
                    'ОК!',
            html: typeof result.value.message != 'undefined' ? 
                  result.value.message :
                  'Ваши данные успешно отправлены',
            confirmButtonText: 'ОК!',
            type: 'success',
            customClass: 'form-submit-success',
            showConfirmButton: false,
            showCancelButton: false,
            showCloseButton: true,
          })
        }
        else {
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Что-то пошло не так при отправке ваших данных!',
          })
        }
      });
  };
  
})(Drupal, window.drupalSettings);