<?php
	
	function wg_theme_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface &$form_state, $form_id = NULL) {
		
		// Work-around for a core bug affecting admin themes. See issue #943212.
		if (isset($form_id)) {
			return;
		}
		
		$form['#attached']['library'][] = 'wg_theme/wg_settings_form';
		
		$form['bg'] = array(
			'#type' => 'details',
			'#title' => t('Region background'),
			'#open' => TRUE,
		);
		
		$form['bg']['default_bg'] = array(
			'#type' => 'managed_file',
			'#title' => t('Default highlighting background'),
			'#default_value' => theme_get_setting('default_bg'),
			'#upload_location' => 'public://bg/',
			'#upload_validators' => array(
				'file_validate_extensions' => array('gif png jpg jpeg'),
				'file_validate_image_resolution' => array('2000x2000'),
			),
		);
		
		$form['bg']['highlighting_bg'] = array(
			'#type' => 'managed_file',
			'#title' => t('Frontpage highlighting background'),
			'#default_value' => theme_get_setting('highlighting_bg'),
			'#upload_location' => 'public://bg/',
			'#upload_validators' => array(
				'file_validate_extensions' => array('gif png jpg jpeg'),
				'file_validate_image_resolution' => array('2000x2000'),
			),
		);
		
		$form['bg']['page_bottom_bg'] = array(
			'#type' => 'managed_file',
			'#title' => t('Page bottom background'),
			'#default_value' => theme_get_setting('page_bottom_bg'),
			'#upload_location' => 'public://bg/',
			'#upload_validators' => array(
				'file_validate_extensions' => array('gif png jpg jpeg'),
				'file_validate_image_resolution' => array('2000x2000'),
			),
		);
		
		$form['bg']['footer_bg'] = array(
			'#type' => 'managed_file',
			'#title' => t('Footer background'),
			'#default_value' => theme_get_setting('footer_bg'),
			'#upload_location' => 'public://bg/',
			'#upload_validators' => array(
				'file_validate_extensions' => array('gif png jpg jpeg'),
				'file_validate_image_resolution' => array('2000x2000'),
			),
		);
		
		$form['menu'] = array(
			'#type' => 'details',
			'#title' => t('Fixed top menu'),
			'#open' => TRUE,
		);
		
		$form['menu']['menu_fixed'] = array(
			'#type' => 'radios',
			'#title' => '',
			'#default_value' => theme_get_setting('menu_fixed'),
			'#options' => array(0 => t('No'), 1 => t('Fix')),
		);
		
		$form['region'] = array(
			'#type' => 'fieldset',
			'#title' => t('Region settings'),
		);
		
		$form['region']['triptych'] = array(
			'#type' => 'details',
			'#title' => t('Region triptych'),
			'#open' => false,
		);
    
    $form['region']['triptych']['triptych_section_width'] = array(
      '#type' => 'radios',
      '#title' => 'Section width',
      '#default_value' => theme_get_setting('triptych_section_width'),
      '#options' => array(0 => t('Limited width'), 1 => t('Full width')),
    );
		
		$form['region']['triptych']['triptych_cols'] = array(
			'#type' => 'radios',
			'#title' => 'Column position',
			'#default_value' => theme_get_setting('triptych_cols'),
			'#options' => array(0 => t('Type 1'), 1 => t('Type 2'), 2 => t('Type 3'), 3 => t('Type 4')),
      '#attributes' => array(
        'class' => array('layout-radios')
      )
		);
		
		$form['region']['postfix'] = array(
			'#type' => 'details',
			'#title' => t('Postfix region'),
			'#open' => false,
		);
		
		$form['region']['postfix']['postfix_cols'] = array(
			'#type' => 'radios',
			'#title' => 'Column position',
			'#default_value' => theme_get_setting('postfix_cols'),
			'#options' => array(0 => t('Type 1'), 1 => t('Type 2'), 2 => t('Type 3'), 3 => t('Type 4')),
      '#attributes' => array(
        'class' => array('layout-radios')
      )
		);
		
		$form['region']['footer'] = array(
			'#type' => 'details',
			'#title' => t('Footer region'),
			'#open' => false,
		);
		
		$form['region']['footer']['footer_cols'] = array(
			'#type' => 'radios',
			'#title' => 'Column position',
			'#default_value' => theme_get_setting('footer_cols'),
			'#options' => array(0 => t('Type 1'), 1 => t('Type 2'), 2 => t('Type 3'), 3 => t('Type 4')),
      '#attributes' => array(
        'class' => array('layout-radios')
      )
		);
		
		$theme_settings_path = drupal_get_path('theme', 'wg_theme') . '/theme-settings.php';
		if (!in_array($theme_settings_path, $form_state->getBuildInfo()['files'])) {
			$newBuildInfo = $form_state->getBuildInfo();
			$newBuildInfo['files'][] = $theme_settings_path;
			$form_state->setBuildInfo($newBuildInfo);
		}
		
		$form['#submit'][] = 'wg_theme_form_system_theme_settings_submit';
	}
	
	/**
	 * Check and save the uploaded background image
	 */
	function wg_theme_form_system_theme_settings_submit(&$form, \Drupal\Core\Form\FormStateInterface &$form_state) {
		
		$values = $form_state->getValues();
		
		$fid = [];
		
		if (!empty($values['highlighting_bg'])) {
			$fid[] = $values['highlighting_bg'][0];
		}
		
		if (!empty($values['page_bottom_bg'])) {
			$fid[] = $values['page_bottom_bg'][0];
		}
		
		if (!empty($values['footer_bg'])) {
			$fid[] = $values['footer_bg'][0];
		}
		
		if (!empty($values['default_bg'])) {
			$fid[] = $values['default_bg'][0];
		}
		
		wg_theme_img_load($fid);
	}
	
	function wg_theme_img_load($fids) {
		
		if (isset($fids)) {
			foreach ($fids as $fid) {
				if ($fid) {
					$file = \Drupal\file\Entity\File::load($fid);
					
					if (!($file->isPermanent())) {
						$file->setPermanent();
						$file->save();
					}
					
					$file_usage = \Drupal::service('file.usage')->listUsage($file);
					
					if (isset($file_usage['file'])) {
						if (isset($file_usage['file']['block'])) {
							if (isset($file_usage['file']['block']['theme_bg'])) {
								continue;
							}
						}
					} else {
						\Drupal::service('file.usage')->add($file, 'file', 'block', 'theme_bg');
					}
				}
			}
		}
		
		// Remove usage for all other files (removed with "Remove") button.
		$files_usages = \Drupal::database()
			->select('file_usage', 'u')
			->fields('u')
			->condition('u.module', 'file', '=')
			->condition('u.type', 'block', '=')
			->condition('u.id', 'theme_bg', '=')
			->execute();
		
		$fids_usages_to_delete = array();
		
		while ($file_usage = $files_usages->fetchAssoc()) {
			
			// Skip the file if it's really used by gd_header block configuration.
			if (isset($fids)) {
				
				if (in_array($file_usage['fid'], $fids)) {
					continue;
				} else {
					$fids_usages_to_delete[] = $file_usage['fid'];
				}
			} else {
				$fids_usages_to_delete[] = $file_usage['fid'];
			}
		}
		
		foreach ($fids_usages_to_delete as $fid_usage_to_delete) {
			$file_to_delete = \Drupal\file\Entity\File::load($fid_usage_to_delete);
			
			// Delete the file usage.
			\Drupal::service('file.usage')->delete($file_to_delete, 'file', 'block', 'theme_bg');
			
			// Set the file as temporary so it can be deleted by Drupal.
			$file_to_delete->setTemporary();
			$file_to_delete->save();
		}
		
	}
