(function($) {
  $(document).ready(function() {

    $('.block-open-menu').on('click', function() {
      if(!$('a').hasClass('close-menu')){
        $('.top-menu-wrapper .navigation .menu').prepend('<li class="close-menu-li"><a href="#" class="close-menu">Закрыть</a></li>');
      }
      $('.top-menu-wrapper .block-menu').animate({
        left: "0px"
      }, 200);
    });

    $('body, html').on('click', '.close-menu', function(e) {
      e.preventDefault();
      $('.top-menu-wrapper .block-menu').animate({
        left: "-285px"
      }, 200);
      if($('a').hasClass('close-menu')){
        $('.close-menu-li').remove();
      }
    });

		if ($('div').hasClass('menu-fixed-top')) {
			var objToStick = $(".top-menu-wrapper");
			var topOfObjToStick = objToStick.offset().top;

			$(window).scroll(function () {
				var windowScroll = $(window).scrollTop();

				if (windowScroll > topOfObjToStick) {
					objToStick.addClass("fixed");
					var objCurentHeight = objToStick.height();
					$('#page').css({'margin-top' : objCurentHeight});
				} else {
					objToStick.removeClass("fixed");
					$('#page').css({'margin-top' : ''});
				};
			});
		}

  });

  Drupal.behaviors.mes = {
    attach: function (context) {
      $('.mes-close').once('mes').on('click', function(){
        $(this).parents('.stat-message').remove();
      });
    }
  };

})(jQuery);